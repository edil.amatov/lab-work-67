class Dish < ApplicationRecord
	belongs_to :shop
	has_one_attached :picture
end
