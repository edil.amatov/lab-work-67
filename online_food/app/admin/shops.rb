ActiveAdmin.register Shop do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :title, :description, :picture
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  form do |f|
    f.inputs do
      f.input :title
      f.input :description
      if f.object.picture.attached?
        f.input :picture,
        :as => :file,
        :hint => image_tag(
          url_for(
            f.object.picture.variant(
              combine_options: {
                gravity: 'Center',
                crop: '50x50+0+0'
              }
              )
            )
          )
      else
        f.input :picture, :as => :file
      end
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :picture do |shop|
      image_tag shop.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
    end
    column :title do |shop|
      link_to shop.title, admin_shop_path(shop)
    end
    actions
  end

  show do
    attributes_table do
      row :image do |shop|
        image_tag shop.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0' })
      end
      row :title
      row :description
    end
    active_admin_comments
  end
  
end
