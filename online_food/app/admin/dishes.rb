ActiveAdmin.register Dish do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :shop_id, :title, :description, :price, :picture
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end

  form do |f|
    f.inputs do
      f.input :shop
      f.input :title
      f.input :description
      f.input :price, min: 0
      if f.object.picture.attached?
        f.input :picture,
        :as => :file,
        :hint => image_tag(
          url_for(
            f.object.picture.variant(
              combine_options: {
                gravity: 'Center',
                crop: '50x50+0+0'
              }
              )
            )
          )
      else
        f.input :picture, :as => :file
      end
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :picture do |dish|
      image_tag dish.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0+0' })
    end
    column :title do |dish|
      link_to dish.title, admin_dish_path(dish)
    end
    column :price
    column :shop
    actions
  end

  show do
    attributes_table do
      row :image do |dish|
        image_tag dish.picture.variant(combine_options: { gravity: 'Center', crop: '50x50+0' })
      end
      row :title
      row :shop
      row :price
      row :description
    end
    active_admin_comments
  end
  
end
