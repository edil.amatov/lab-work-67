class ShopsController < ApplicationController

	def index
		@shops = Shop.all
	end

	def show
		dishes = Dish.all
		@shop = Shop.find(params[:id])
		@dishes = @shop.dishes
	end


end
